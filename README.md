# OpenML dataset: papir_2

https://www.openml.org/d/487

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Magne Aldrin (magne.aldrin@nr.no)  
**Source**: [StatLib](http://lib.stat.cmu.edu/datasets/) - April 14. 1999  
**Please cite**:   

One of two multivariate regression data sets from paper industry, from an experiment at the paper plant Saugbruksforeningen, Norway. They have been described and analysed in:  
Aldrin, M. (1996), "Moderate projection pursuit regression for multivariate response data", Computational Statistics and Data Analysis,
21, p. 501-531.

It consists of 30 observations (rows) and 41 variables (columns). Columns 1 to 32 are response variables that describes various qualities 
of the paper. Columns 33 to 41 are 9 predictor variables. The first three predictor variables  (x1 in column 33, x2 in column 34 and x3 in column 35) were varied systematically through the experiment. The next three predictor variables (columns 36 to 38) are constructed as x1**2, x2**2 and x3**2. The last three predictor variables (columns 39 to 41) are constructed as x1*x2, x1*x3 and x2*x3.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/487) of an [OpenML dataset](https://www.openml.org/d/487). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/487/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/487/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/487/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

